#include <avr/io.h>

/*
	Setup SPI
*/
void spi_setup(void) {
	
	/**** EXTERNAL INTERRUPT *****/
	DDRD &= ~(1<<DDRD2); //setup External interrupt 0 as Input (PD2) (clear bit 2)
	PORTD |= (1<<PD2); //set PD2(INT0) to high (impedance) (pull-up resistor is activated)
	EIMSK |= (1<<INT0);  //enable INT0
	EICRA |= (1<<ISC01); //INT0 triggered on falling edge

	/********** SPI ***********/
	DDRB |= (1<<DDRB4); // MISO output
	SPCR = (1<<SPE); // Enable SPI

}

/* 
	Perform one byte spi transmission
	tx: The byte to send
	Returns the received byte
*/
uint8_t spi_tx_rx(uint8_t tx) {
	SPDR = tx; // Load data into microcontroller register
	while(!(SPSR & (1<<SPIF))); // Wait for transmission to complete
	return SPDR;
}