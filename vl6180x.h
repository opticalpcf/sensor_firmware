#ifndef VL6180X_h
#define VL6180X_h

#include "pca9554b.h"

typedef struct _vl6180x
{
	uint8_t addr; // The slave address of the sensor
	uint8_t transmit[4]; // Buffer for transmiting data over I2C
	uint8_t receive[3]; // Buffer for receiving data over I2C
	int8_t port_B_pin; // The pin used as GPIO0 on PORTB. -1 if not used
	int8_t port_C_pin; // The pin used as GPIO0 on PORTC. -1 if not ued
	int8_t port_D_pin; // The pin used as GRPI0 in PORTD. -1 if not used
	pca9554b* expander; // The expander used to toggle GPIO0, 0 if not used
	int8_t expander_id; // The pin on the expander this device is connected to, -1 if not used 
}vl6180x;

/* 
	Initialize a VL6180X sensor
	vl6Ptr: The device to be initialized
	address: The I2C slave address to assign to the device
*/
uint8_t vl6180x_init(vl6180x * vl6Ptr, uint8_t address);

/*
	Change the I2C slave address of the device
	vlPtr: The device whose address should be changed
	new_addr: The desired I2C address
*/
uint8_t change_address(vl6180x * vl6Ptr, uint8_t new_addr);

/*
	Toggle measurement mode on and off
	vl6Ptr: The device whose measurement should be toggled
*/
void toggle_measurement(vl6180x * vl6Ptr);

// Get data from vl6Ptr. Set range_meas to 1 to get a range measurement, 0 for ALS measurement
// Returns 0 if the new and previous measurement were equal
//uint8_t get_data(vl6180x * vl6Ptr, uint8_t range_meas);

// Initialize interleave mode
//void start_interleave(vl6180x * vl6Ptr);

/*
void set_register(vl6180x* vl6Ptr, uint8_t reg_addr, uint8_t* reg_data, uint8_t data_length);
void read_register(vl6180x* vl6Ptr, uint8_t reg_addr, uint8_t* reg_data, uint8_t data_length);
*/

//void enable_sensitivity(vl6180x* vl6Ptr);
//void set_sensitivity(vl6180x* vl6Ptr, uint8_t sensitivity_msb, uint8_t sensitivty_lsb);

#endif