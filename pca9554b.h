#ifndef PCA9554B_H
#define PCA9554B_H

#include <avr/io.h>

struct _vl6180x; // forward declaration

typedef struct
{
	uint8_t addr; // The slave address of the gpio expander
	uint8_t cur_device_output; // The current output of the device
}pca9554b;

/*
	Initialize the gpio expander
*/
void pca9554b_setup(pca9554b* expander);

/*
	Set the pin connected to the passed VL6180X device to be high
	vl6Ptr: The device that should be brought out of reset
*/
void pca9554b_set_pin(struct _vl6180x* vl6Ptr);

/*
	Set the pin connected to the passed VL6180x device to be low
	vl6Ptr: The device that should be put into reset
*/
void pca9554b_reset_pin(struct _vl6180x* vl6Ptr);

#endif