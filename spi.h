#ifndef SPI_H
#define SPI_H

/*
	Setup SPI
*/
void spi_setup(void);

/* 
	Perform one byte spi transmission
	tx: The byte to send
	Returns the received byte
*/
uint8_t spi_tx_rx(uint8_t tx);

#endif