#include "pca9554b.h"
#include "i2c.h"
#include "vl6180x.h"


/*
	Initialize the gpio expander
*/
void pca9554b_setup(pca9554b* expander) {

	write_byte_pca9554(expander->addr, 0x03, 0x00); // Set all pins to be outputs
	write_byte_pca9554(expander->addr, 0x01, 0x00); // Pull all pins low 
}

/*
	Set the pin connected to the passed VL6180X device to be high
	vl6Ptr: The device that should be brought out of reset
*/
void pca9554b_set_pin(vl6180x* vl6Ptr) {
	if(vl6Ptr->expander == 0 || vl6Ptr->expander_id < 0 || vl6Ptr->expander_id > 7) {
		return;
	}
	vl6Ptr->expander->cur_device_output |= (1<<vl6Ptr->expander_id);
	write_byte_pca9554(vl6Ptr->expander->addr, 0x01, vl6Ptr->expander->cur_device_output);

}

/*
	Set the pin connected to the passed VL6180x device to be low
	vl6Ptr: The device that should be put into reset
*/
void pca9554b_reset_pin(vl6180x* vl6Ptr) {
	if(vl6Ptr->expander == 0 || vl6Ptr->expander_id < 0 || vl6Ptr->expander_id > 7) {
		return;
	}
	vl6Ptr->expander->cur_device_output &= ~(1<<vl6Ptr->expander_id);
	write_byte_pca9554(vl6Ptr->expander->addr, 0x01, vl6Ptr->expander->cur_device_output);

}