#ifndef I2C_h

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#define I2C_h

#include "vl6180x.h"

/*
	Initialize I2C
*/
void TWI_Master_Initialise(void);

/*
	 Helper method for I2C
*/
uint8_t TWI_Poll(uint8_t cmd);

/*
	Write data to the reg_addr of the sensor corresponding to vl6Ptr
	vl6Ptr must have slave address in addr field
	vl6Ptr: Device to write to
	reg_addr: Register address to write to
	data: The byte to be written
*/
uint8_t write_byte(vl6180x * vl6Ptr, uint16_t reg_addr, uint8_t data);

/*
	Write a byte to the gpio expander
	addr: I2C Slave address of gpio expander
	reg_addr: Register address of gpio expander
	data: Byte to write to register
*/
uint8_t write_byte_pca9554(uint8_t addr, uint8_t reg_addr, uint8_t data);

/*
	vl6Ptr: Must have transmit buffer loaded with the following
		slave address at index 0 
		register address to write to at indices 1 and 2
		data at subsequent indices
	n: The number of bytes to write to vl6Ptr (including slave and reg address)
*/
uint8_t write_bytes(vl6180x * vl6Ptr, uint8_t n);

/*
	Use I2C to write bytes to a device
	slave_addr: I2C address of device
	reg_addr: Device register address to write to
	data: Array of data to write
	n: Number of bytes to write
*/
uint8_t write_bytes_generic(uint8_t slave_addr, uint16_t reg_addr, uint8_t* data, uint8_t n);

/*
	 vl6Ptr: Must have transmit buffer loaded with the following
		slave address at index 0
		register address to read from at indices 1 and 2
		It must also have the receive buffer loaded with the (read) slave address at the first index
		Will have data in subsequent bit after read
	n: The number of bytes to read from vl6Ptr
*/
uint8_t read_bytes(vl6180x * vl6Ptr, uint8_t n);

/*
	Read from a device over I2C
	slave_addr: The I2C address of the device
	reg_addr: Device register address to write to
	data: The data to write
	n: The number of data bytes to write
*/
uint8_t read_bytes_generic(uint8_t slave_addr, uint16_t reg_addr, uint8_t* data, uint8_t n);

#endif