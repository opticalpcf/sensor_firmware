/*
 * OpticalWeiss.c
 *
 * Created: 11/4/2017 6:35:59 PM
 * Author : Patrick
 */ 
#define F_CPU 20000000
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>     
#include <avr/wdt.h>                                   
#include "vl6180x.h"
#include "spi.h"
#include "i2c.h"

#define ARRAY_QUEUE_SIZE 3 // The number of sensor arrays being used
#define TOP_ARRAY_IDX 0 // If array is not used, set idx to value above ARRAY_QUEUE_SIZE
#define FRONT_ARRAY_IDX 1
#define BOTTOM_ARRAY_IDX 2

// TOP ARRAY refers to the top slave pcb
// FRONT ARRAY refers to the front slave pcb
// BOTTOM ARRAY refers to the bottom slave pcb

#define MAX_DEVICE_COUNT 8 // The maximum number of devices a single array can contain
#define TOP_ARRAY_DEV_COUNT 8 // Number of devices in top array
#define FRONT_ARRAY_DEV_COUNT 1 // Number of devices in front array
#define BOTTOM_ARRAY_DEV_COUNT 8 // Number of devices in bottom array

#define VL6_DEF_SLAVE_ADDR 0x52	 // The default sensor (VL6180X) I2C device address
#define PCA_TOP_SLAVE_ADDR 0x42 // The I2C address of the top array's gpio expander
#define PCA_BOTTOM_SLAVE_ADDR 0x40 // The I2C address of the bottom array's gpio expander

// MACROS for received SPI commands
// Note each array keeps a separate list of registers
// This list is referenced when a read or write is requested
#define SPI_SET_REG 0  // Empty the register list and then add a register
#define SPI_ADD_REG 1 // Add a register to the register list
#define SPI_DEL_REG 2 // Remove a register from the register list
#define SPI_READ_REG 3 // Read from the registers in the register list
#define SPI_WRITE_REG 4 // Write to the registers in the register list
#define SPI_CALIB_RESULT 5 // Get the result of the most recent calibration
#define SPI_CALIB_SEE_THROUGH 6 // Set minimum return rate so that max reading is reported when
								// insufficient signal is received
#define SPI_CALIB_SEE_DARK 7    // Not used
#define SPI_CALIB_OFFSET 8      // Calibrate for offset in measurements
#define SPI_CALIB_CROSS_TALK 9  // Calibrate scale of measurements, including for dark targets

#define RANGE_REG 0x0062 // VL6180X range measurement register address
#define RANGE_RATE_REG 0x0066 // VL6180x range rate measurement register address
#define RANGE_RATE_THRESH_REG 0x0026 // VL6180X register for setting the required signal level
#define INTERMEASUREMENT_REG 0x001B // VL6180X register for time between range measurements 
#define MAX_CONVERGENCE_REG 0x001C // VL6180X register for max time length of measurement 
#define RANGE_OFFSET_REG 0x0024 // VL6180x register for range offset calibration value
#define CROSSTALK_COMPENSATION_REG 0x001E // VL6180X register for cross-talk calibration value
#define MAX_INTERMEASUREMENT_PERIOD_MS 76 // The longest possible time between measurements 
										  // (not a hardware limitation, is enforced by software settings)

#define MAX_REGISTERS 5 // The maximum number of registers in the register list

// Data structure that represents a single array
typedef struct _vl6_array{
	uint8_t device_count; // Number of devices in array
	vl6180x device_queue[MAX_DEVICE_COUNT]; // The devices in the array
	uint16_t cur_reg_addrs[MAX_REGISTERS]; // The addresses in the register list
	uint8_t cur_reg_lengths[MAX_REGISTERS]; // The lengths of registers in the register list
	uint8_t cur_reg_count; // The number of registers in the register list
	uint8_t cur_data[4*MAX_REGISTERS*MAX_DEVICE_COUNT];  // Buffer for serialized register data
	uint8_t calib_result[2*MAX_DEVICE_COUNT]; // Buffer for results of calibration
}vl6_array;

volatile uint8_t timer_flag; // Set every 30 ms by timer
volatile uint8_t calib_flag; // Indicates that calibration has been requested
volatile uint8_t spi_flag; // Indicates that spi transmission has been requested
volatile uint8_t cur_cmd; // The most recent received spi cmd
volatile uint8_t cur_array; // The most recently reqested array. Used for indexing 'array_queue'
volatile uint8_t calib_samples; // Number of samples to use in calibration (specified through SPI)
volatile uint8_t calib_target; // The target distance used in calibration (specified through SPI)
volatile vl6_array array_queue[ARRAY_QUEUE_SIZE]; // An array that holds all of the sensor arrays

void val_to_array(uint16_t val, uint8_t* arr, uint8_t arr_length); // Converts a 16 bit value into a byte array
void array_to_val(uint8_t* arr, uint8_t arr_length, uint16_t* val); // Converts a byte array into a 16 bit value
void setup_timer1(void); // Initializes the timer
void calib_see_through(vl6_array* dev_array); // Performs calibration to find acceptable measurement signal threshold
void calib_see_dark(vl6_array* dev_array); // Not used
void calib_offset(vl6_array* dev_array); // Performs calibration to find measurement offset
void calib_cross_talk(vl6_array* dev_array); // Performs measurement to find crosstalk value for invariance to target color

/*
	Perform initialization of sensor, respond to data requests, and read VL6180X devices
*/
int main(void) {
	wdt_reset(); // Reset watch dog timer					
	MCUSR = 0x00; // Get ready to update watch dog timer					
	WDTCSR |= (1<<WDCE) | (1<<WDE);	 // Enable changing of watch dog timer and reset it
	//WDTCR = (1<<WDP2) | (1<<WDP1);	// Set prescale for 128K (~1.0 sec) with 0110 in WDP3-WDP0
	asm("LDI R16, 0x0E");	// Get our value (1<<WDP2) | (1<<WDP1) into R18 (1 clock)
	asm("STS 0x60, R16");	// Save in WDTCR (address 0x21) (1 clock)

	// Initialize flags
	timer_flag = 0;
	calib_flag = 0;
	spi_flag = 0;
	
	spi_setup(); // Setup SPI
	TWI_Master_Initialise(); // Setup I2C

	_delay_ms(1);
	wdt_reset(); 

	// Initialize the top gpio expander device
	pca9554b top_expander;
	top_expander.addr = PCA_TOP_SLAVE_ADDR;
	top_expander.cur_device_output = 0xff;
	pca9554b_setup(&top_expander);

	// Initialize the top sensor array
	array_queue[TOP_ARRAY_IDX].device_count = 0;
	if(TOP_ARRAY_IDX < ARRAY_QUEUE_SIZE) {
		_delay_ms(10);
		wdt_reset();

		array_queue[TOP_ARRAY_IDX].device_count = TOP_ARRAY_DEV_COUNT; // Set to correct device count
		array_queue[TOP_ARRAY_IDX].cur_reg_count = 0; // Initialize number of registers in register list

		for(int i = 0; i < array_queue[TOP_ARRAY_IDX].device_count; i++) {
			// Set the top expander as the method for initializing top array
			array_queue[TOP_ARRAY_IDX].device_queue[i].port_B_pin = -1;
			array_queue[TOP_ARRAY_IDX].device_queue[i].port_C_pin = -1;
			array_queue[TOP_ARRAY_IDX].device_queue[i].port_D_pin = -1;
			array_queue[TOP_ARRAY_IDX].device_queue[i].expander = &top_expander;

			// Devices are not in descending order in hardware, so we correct for this in software
			if(i < 4) {
				array_queue[TOP_ARRAY_IDX].device_queue[i].expander_id = i;
			} else {
				array_queue[TOP_ARRAY_IDX].device_queue[i].expander_id = (array_queue[TOP_ARRAY_IDX].device_count - 1)+(4-i);
			}
		}

	}
	
	// Initialize the front sensor array
	array_queue[FRONT_ARRAY_IDX].device_count = 0;
	if(FRONT_ARRAY_IDX < ARRAY_QUEUE_SIZE && FRONT_ARRAY_DEV_COUNT == 1) {
		array_queue[FRONT_ARRAY_IDX].device_count = FRONT_ARRAY_DEV_COUNT; // Set to correct device count

		array_queue[FRONT_ARRAY_IDX].cur_reg_count = 0; // Initialize number of registers in register list

		for(int i = 0; i < array_queue[FRONT_ARRAY_IDX].device_count; i++) {
			// Set microcontroller gpio D6 as method for initializing array
			// Note that this array should only have one device in it
			array_queue[FRONT_ARRAY_IDX].device_queue[i].port_B_pin = -1;
			array_queue[FRONT_ARRAY_IDX].device_queue[i].port_C_pin = -1;
			array_queue[FRONT_ARRAY_IDX].device_queue[i].port_D_pin = 6;
			array_queue[FRONT_ARRAY_IDX].device_queue[i].expander = 0;
			array_queue[FRONT_ARRAY_IDX].device_queue[i].expander_id = -1;
		}		
	}

	// Initialize the bottom gpio expander device
	pca9554b bottom_expander;
	bottom_expander.addr = PCA_BOTTOM_SLAVE_ADDR;
	bottom_expander.cur_device_output = 0xff;
	pca9554b_setup(&bottom_expander);

	// Initialize the bottom sensor array
	array_queue[BOTTOM_ARRAY_IDX].device_count = 0;
	if(BOTTOM_ARRAY_IDX < ARRAY_QUEUE_SIZE) {
		
		_delay_ms(10);
		wdt_reset();

		array_queue[BOTTOM_ARRAY_IDX].device_count = BOTTOM_ARRAY_DEV_COUNT; // Set to correct device count
		array_queue[BOTTOM_ARRAY_IDX].cur_reg_count = 0; // Initialize number of registers in register list
		
		for(int i = 0; i < array_queue[BOTTOM_ARRAY_IDX].device_count; i++) {
			// Set bottom gpio expander as method for initializing array
			array_queue[BOTTOM_ARRAY_IDX].device_queue[i].port_B_pin = -1;
			array_queue[BOTTOM_ARRAY_IDX].device_queue[i].port_C_pin = -1;
			array_queue[BOTTOM_ARRAY_IDX].device_queue[i].port_D_pin = -1;
			array_queue[BOTTOM_ARRAY_IDX].device_queue[i].expander = &bottom_expander;
			// Devices are not in descending order in hardware, so we correct for this in software
			if(i < 4) {
				array_queue[BOTTOM_ARRAY_IDX].device_queue[i].expander_id = i;
				} else {
				array_queue[BOTTOM_ARRAY_IDX].device_queue[i].expander_id = (array_queue[BOTTOM_ARRAY_IDX].device_count - 1)+(4-i);
			}
		}
	}

	// Pull all VL6180X GPIO 0 low for first step of re-addressing each device
	for(int i = 0; i < ARRAY_QUEUE_SIZE; i++) {
		for(int j = 0; j < array_queue[i].device_count; j++) {
			if(array_queue[i].device_queue[j].expander != 0 && array_queue[i].device_queue[j].expander_id >= 0 && array_queue[i].device_queue[j].expander_id <= 7) {
				pca9554b_reset_pin(&(array_queue[i].device_queue[j])); // Expander will pull VL6180X gpio low
			} else if(array_queue[i].device_queue[j].port_B_pin >= 0) {
				PORTB &= ~(1<<(array_queue[i].device_queue[j].port_B_pin));
				DDRB |=   (1<<array_queue[i].device_queue[j].port_B_pin); // Microcontroller will pull VL6180X gpio low through port b pin
			} else if(array_queue[i].device_queue[j].port_C_pin >= 0) {
				PORTC &= ~(1<<(array_queue[i].device_queue[j].port_C_pin));
				DDRC |=   (1<<array_queue[i].device_queue[j].port_C_pin); // Microcontroller will pull VL6180X gpio low through port c pin
			} else if(array_queue[i].device_queue[j].port_D_pin >= 0) {
				PORTD &= ~(1<<(array_queue[i].device_queue[j].port_D_pin));
				DDRD |=   (1<<array_queue[i].device_queue[j].port_D_pin); // Microcontroller will pull VL6180X gpio low through port d pin
			}
			_delay_ms(10);
			wdt_reset();	
		}
	}

	_delay_ms(2);
	
	// One-by-one, pull VL6180X out of reset and assign to unique address
	uint8_t init_count = 0;
	for(int i = 0; i < ARRAY_QUEUE_SIZE; i++) {
		for(int j = 0; j < array_queue[i].device_count; j++) {
			if(array_queue[i].device_queue[j].expander != 0 && array_queue[i].device_queue[j].expander_id >= 0 && array_queue[i].device_queue[j].expander_id <= 7) {
				pca9554b_set_pin(&(array_queue[i].device_queue[j])); // Expander unresets device
			} else if(array_queue[i].device_queue[j].port_B_pin >= 0) {
				DDRB &= ~(1<<(array_queue[i].device_queue[j].port_B_pin)); // Microcontroller unreset device by allowing port B input to float 
			} else if(array_queue[i].device_queue[j].port_C_pin >= 0) {
				DDRC &= ~(1<<(array_queue[i].device_queue[j].port_C_pin));  // Microcontroller unreset device by allowing port C input to float
			} else if(array_queue[i].device_queue[j].port_D_pin >= 0) {
				DDRD &= ~(1<<(array_queue[i].device_queue[j].port_D_pin));  // Microcontroller unreset device by allowing port D input to float
			}
			_delay_ms(10);
			wdt_reset();

			// Configure the sensor
			vl6180x_init(&(array_queue[i].device_queue[j]), VL6_DEF_SLAVE_ADDR + 2*(init_count+1)); // Initialize VL6180X device with unique address
			init_count += 1;
		}

	}

	_delay_ms(1);
	wdt_reset();
	setup_timer1(); // Initialize timer
	sei(); // Enable interrupts
	
	// Offset initialization of measurements from each device to try to de-correlate inter-device noise
	for(int i = 0; i < ARRAY_QUEUE_SIZE; i++) {
		for(int j = 0; j < array_queue[i].device_count; j++) {
			while(timer_flag != 1); // Wait 30 ms
			for(int k = 0; k < j; k++) { // Additional delay depending on device idx
				_delay_us(100);
			}
			toggle_measurement(&(array_queue[i].device_queue[j])); // Start measuring
			timer_flag = 0;
		}
	}
	
	// Main loop
    while (1) {
		wdt_reset();

		// Check if calibration has been requested
		if(calib_flag == 1) {
			// Reset calibration result array
			for(unsigned int i = 0; i < 2*array_queue[cur_array].device_count; i++) {
				array_queue[cur_array].calib_result[i] = 0;
			}
			switch(cur_cmd) {
			case SPI_CALIB_SEE_THROUGH:
			{
				calib_see_through(&(array_queue[cur_array])); // Perform see through calibration
				break;
			}
			case SPI_CALIB_SEE_DARK:
			{
				calib_see_dark(&(array_queue[cur_array])); // Not used
				break;
			}
			case SPI_CALIB_OFFSET:
			{
				calib_offset(&(array_queue[cur_array])); // Perform offset calibration
				break;
			}
			case SPI_CALIB_CROSS_TALK:
				calib_cross_talk(&(array_queue[cur_array])); // Perform cross-talk calibration
				break;
			}
			calib_flag = 0; // Reset flag
			
		}

		// Check if timer duration has passed
		if(timer_flag == 1) {
			// Iterate through each device, reading the registers in the corresponding register list
			for(unsigned int i = 0; i < ARRAY_QUEUE_SIZE; i++) {
				uint32_t data_count = 0;
				for(unsigned int j = 0; j < array_queue[i].device_count; j++) {
					for(unsigned int k = 0; k < array_queue[i].cur_reg_count; k++) {
						// Read register and serialize into device array's data buffer for quick transmission
						read_bytes_generic(array_queue[i].device_queue[j].addr, array_queue[i].cur_reg_addrs[k], array_queue[i].cur_data+data_count, array_queue[i].cur_reg_lengths[k]);
						data_count += array_queue[i].cur_reg_lengths[k];
					}
					
				}
			}

			timer_flag = 0;			
		}
    }
}

/*
	Convert a byte array to a 16 bit value
	arr: A byte array with either one or two elements
	arr_length: The number of valid elements in arr (should be 1 or 2)
	val: The resulting 16 bit value
*/
void array_to_val(uint8_t* arr, uint8_t arr_length, uint16_t* val) {
	*val = 0;
	for(int i = 0; i < arr_length; i++) {
		*val += arr[arr_length-1-i] << (8*i);
	}
}

/*
	Convert a 16 bit value to a byte array
	val: The 16 bit value to convert
	arr: The byte array to restore the result into
	arr_length: The length of arr
*/
void val_to_array(uint16_t val, uint8_t* arr, uint8_t arr_length) {
	for(int i = 0; i < arr_length; i++) {
		arr[arr_length-1-i] = 0x00FF & (val >> 8*i);
	}
}

/*
	Initialize the timer to interrupt every 30 ms
*/
void setup_timer1(void) {
	// Set up timer with prescaler = 64 and CTC mode
	TCCR1B |= (1 << WGM12)|(1 << CS11)|(1 << CS10);
	
	// Initialize counter
	TCNT1 = 0;
	
	// Initialize compare value
	OCR1A = 9374;
	
	// Enable compare interrupt
	TIMSK1 |= (1 << OCIE1A);
}

/*
	Perform calibration to find the minimum measurement signal threshold
	Pre-Condition: No target is in front of any of the devices
	dev_array: The devices to be calibrated

*/
void calib_see_through(vl6_array* dev_array) {
	wdt_reset();
	uint16_t max_range_rate; // The max range rate seen so far
	uint16_t range_rate; // The current range rate value
	uint8_t range_rate_arr[2]; // Array representation of range rate value

	// Repeat for each VL6180X device
	for(unsigned int i = 0; i < dev_array->device_count; i++) {
		max_range_rate = 0; // Set to min val
		for(unsigned int j = 0; j < calib_samples; j++) {
			wdt_reset();
			read_bytes_generic(dev_array->device_queue[i].addr, RANGE_RATE_REG, range_rate_arr, 2); // Get range rate reading
			array_to_val(range_rate_arr, 2, &range_rate); // Convert byte array to 16 bit val
			if(range_rate > max_range_rate) { // Update max range rate
				max_range_rate = range_rate;
			}
			_delay_ms(30);
		}
		max_range_rate = (uint16_t) (1.1*max_range_rate); // Require range rate to be 10% higher than max observede for valid measurement
		val_to_array(max_range_rate, range_rate_arr, 2); // Convert 16 bit value to byte array
		write_bytes_generic(dev_array->device_queue[i].addr, RANGE_RATE_THRESH_REG, range_rate_arr, 2); // Write the calibration result to device register
		dev_array->calib_result[2*i] = range_rate_arr[0]; // Store calibration result in calibration result array
		dev_array->calib_result[2*i+1] = range_rate_arr[1];
					
	}
	wdt_reset();
}

/*
	Not used
*/
void calib_see_dark(vl6_array* dev_array) {
	wdt_reset();
	uint8_t cur_intermeasurement_period;
	uint8_t cur_max_convergence;
	uint8_t range;
	uint8_t range_tmp;
	for(unsigned int i = 0; i < dev_array->device_count; i++) {
		read_bytes_generic(dev_array->device_queue[i].addr, INTERMEASUREMENT_REG, &cur_intermeasurement_period, 1);
		read_bytes_generic(dev_array->device_queue[i].addr, MAX_CONVERGENCE_REG, &cur_max_convergence, 1);
		cur_max_convergence = 0x3F & cur_max_convergence;
		range = 255;
		while(range == 255) {
			if(cur_max_convergence >= 0x3F || cur_intermeasurement_period >= 0xFF) {
				break;
			}
			cur_max_convergence += 1;
			while( 0.9*10*(cur_intermeasurement_period+1) < cur_max_convergence+5) {
				cur_intermeasurement_period += 1;
			}
			/*
			toggle_range(device_queue[i]);
			_delay_ms(1);
			*/
			write_bytes_generic(dev_array->device_queue[i].addr, INTERMEASUREMENT_REG, &cur_intermeasurement_period, 1);
			write_bytes_generic(dev_array->device_queue[i].addr, MAX_CONVERGENCE_REG, &cur_max_convergence, 1);
			/*
			_delay_ms(1);
			toggle_range(device_queue[i]);
			*/
			_delay_ms(MAX_INTERMEASUREMENT_PERIOD_MS);
						
			range = 0;
			for(int j = 0; j < calib_samples; j++) {
				wdt_reset();
				read_bytes_generic(dev_array->device_queue[i].addr, RANGE_REG, &range_tmp, 1);
				if(range_tmp > range) {
					range = range_tmp;
				}

				_delay_ms(MAX_INTERMEASUREMENT_PERIOD_MS);
			}
						
						
		}

		dev_array->calib_result[2*i] = cur_intermeasurement_period;
		dev_array->calib_result[2*i+1] = cur_max_convergence;
					
	}
	wdt_reset();
}

/*
	Perform calibration to find the measurement offset
	Pre-Condition: Target is placed calib_target mm away from sensor
	dev_array: The devices to be calibrated

*/
void calib_offset(vl6_array* dev_array) {
	wdt_reset();

	// Repeat for each device in this array
	for(unsigned int i = 0; i < dev_array->device_count; i++) {
		double range_sum = 0.0;
		uint8_t range = 0;
		uint8_t offset;

		// Set VL6180X range offset register to be zero (so that measurements are unbiased by prev calibration)
		write_bytes_generic(dev_array->device_queue[i].addr, RANGE_OFFSET_REG, &range, 1);

		_delay_ms(MAX_INTERMEASUREMENT_PERIOD_MS); // Wait for device to start taking measurements
		
		// Sum up range measurements
		for(unsigned int j = 0; j < calib_samples; j++) {
			wdt_reset();
			read_bytes_generic(dev_array->device_queue[i].addr, RANGE_REG, &range, 1); // Read range measurement
			range_sum += range;
			_delay_ms(30); // Wait for next measurement
		}
		offset = calib_target - ((uint8_t)((range_sum / calib_samples +0.5))); // Compute offset
		
		write_bytes_generic(dev_array->device_queue[i].addr, RANGE_OFFSET_REG, &offset, 1); // Write calibration result to VL6180X range offset register
		
		dev_array->calib_result[2*i+1] = offset; // Place calibration result in calibration result array
	}
	wdt_reset();
}

/*
	Perform calibration to find the crosstalk (calibrate for dark colored targets)
	Pre-Condition: Target is placed calib_target mm away from sensor
	dev_array: The devices to be calibrated	
*/
void calib_cross_talk(vl6_array* dev_array) {
	wdt_reset();
	for(unsigned int i = 0; i < dev_array->device_count; i++) {
		uint8_t data_arr[2];
		data_arr[0] = 0;
		data_arr[1] = 0;
		uint16_t return_rate = 0; // The most recently measured return rate
		uint16_t cross_talk = 0;  // The cross talk calibration value
		double range_sum = 0.0; // Sum of range measurements
		double return_rate_sum = 0.0; // Sum of range rate measurements

		// Set cross talk calibration register to zero (so that previous calibration doesn't affect this one)
		write_bytes_generic(dev_array->device_queue[i].addr, CROSSTALK_COMPENSATION_REG, data_arr, 2);

		_delay_ms(100);

		for(unsigned int j = 0; j < calib_samples; j++) {
			wdt_reset();
			read_bytes_generic(dev_array->device_queue[i].addr, RANGE_REG, data_arr, 1); // Get range measurement
			range_sum += data_arr[0];
			read_bytes_generic(dev_array->device_queue[i].addr, RANGE_RATE_REG, data_arr, 2); // Get range rate measurement
			array_to_val(data_arr, 2, &return_rate);
			return_rate_sum += return_rate;
			_delay_ms(MAX_INTERMEASUREMENT_PERIOD_MS); // Wait for next measurement
		}
		if(range_sum/calib_samples > calib_target) {
			cross_talk = 0; // Shoulnd't happen - calibration is for range measurements that are too short, rather than too long
		} else {
			cross_talk = (uint16_t) ((return_rate_sum/calib_samples)*(1-(range_sum/calib_samples)/calib_target)+0.5); // Compute cross talk val
		}
		val_to_array(cross_talk, data_arr, 2);
		
		// Write calibration value to VL6180X register
		write_bytes_generic(dev_array->device_queue[i].addr, CROSSTALK_COMPENSATION_REG, data_arr, 2);
		
		dev_array->calib_result[2*i] = data_arr[0]; // Store calibration result in calibration result array
		dev_array->calib_result[2*i+1] =  data_arr[1];
	}
	wdt_reset();
}

/*
	Timer interrupt
*/
ISR (TIMER1_COMPA_vect) {
	wdt_reset();
	timer_flag = 1;
}

/*
	SPI Interrupt, accepts command from host
*/
ISR(INT0_vect) {
	if(timer_flag == 0 && calib_flag == 0) {
		cur_cmd = spi_tx_rx(42); // Get the command from host
		cur_array = spi_tx_rx(43); // Get which array this command is referencing
		switch(cur_cmd) {
		case SPI_SET_REG: // Empty the register list and add register
			array_queue[cur_array].cur_reg_addrs[0] = (spi_tx_rx(44) << 8 | spi_tx_rx(45)); // Add the register address 
			array_queue[cur_array].cur_reg_lengths[0] = spi_tx_rx(46); // Add the register length
			array_queue[cur_array].cur_reg_count = 1; // Reset the number of registers
			break;
		case SPI_ADD_REG: // Add register to the register list
		{
			uint16_t new_address = (spi_tx_rx(44) << 8 | spi_tx_rx(45)); // Get the new register address
			uint8_t new_length = spi_tx_rx(46); // Get the new register length
			if(array_queue[cur_array].cur_reg_count >= MAX_REGISTERS) { // Reset register list if there are tomm many registers
				array_queue[cur_array].cur_reg_addrs[0] = new_address;
				array_queue[cur_array].cur_reg_lengths[0] = new_length;
				array_queue[cur_array].cur_reg_count = 1;
			} else {
				array_queue[cur_array].cur_reg_addrs[array_queue[cur_array].cur_reg_count] = new_address; // Add the register address
				array_queue[cur_array].cur_reg_lengths[array_queue[cur_array].cur_reg_count] = new_length; // Add the register length
				array_queue[cur_array].cur_reg_count += 1; // Increment the number of registers
			}
			break;
		}
		case SPI_DEL_REG: // Remove register from register list
		{
			uint16_t del_address = (spi_tx_rx(44) << 8 | spi_tx_rx(45)); // Get the address of register that should be deleted
			uint8_t i = 0;
			uint8_t j = 0;
			while(i < array_queue[cur_array].cur_reg_count) { // Look for the register to be deleted
				if(array_queue[cur_array].cur_reg_addrs[i] != del_address) { // Shift registers down if necessary
					array_queue[cur_array].cur_reg_addrs[j] = array_queue[cur_array].cur_reg_addrs[i];
					array_queue[cur_array].cur_reg_lengths[j] = array_queue[cur_array].cur_reg_lengths[i];
					j++;
				}
				i++;
			}
			array_queue[cur_array].cur_reg_count = j; // Update register count
			break;
		}
		case SPI_READ_REG:  // Transfer most recent register data back to host
		{
			uint32_t data_count = 0;
			for(unsigned int i = 0; i < array_queue[cur_array].device_count; i++) {
				for(unsigned int j = 0; j < array_queue[cur_array].cur_reg_count; j++) {
					for(unsigned int k = 0; k < array_queue[cur_array].cur_reg_lengths[j]; k++) {
						spi_tx_rx(array_queue[cur_array].cur_data[data_count]);
						data_count += 1;
					}
				}
			}
			break;
		}
		case SPI_WRITE_REG: // Write data to the registers in the register list
		{
			uint8_t device_idx = spi_tx_rx(44); // Get the index of the device to be written to
			// Get the data to be written
			uint32_t data_count = 0;
			for(unsigned int i = 0; i < array_queue[cur_array].cur_reg_count; i++) {
				for(unsigned int j = 0; j < array_queue[cur_array].cur_reg_lengths[i]; j++) {
					array_queue[cur_array].cur_data[data_count] = spi_tx_rx(45+i); 
					data_count += 1;
				}
			}
			
			// Write received data to the device registers
			data_count = 0;
			for(unsigned int i = 0; i < array_queue[cur_array].cur_reg_count; i++) {
				write_bytes_generic(array_queue[cur_array].device_queue[device_idx].addr, array_queue[cur_array].cur_reg_addrs[i], array_queue[cur_array].cur_data+data_count, array_queue[cur_array].cur_reg_lengths[i]);
				data_count += array_queue[cur_array].cur_reg_lengths[i];
			}
			
			break;
		}	
		case SPI_CALIB_RESULT: // Return the results of the most recent calibration
			for(unsigned int i = 0; i < 2*array_queue[cur_array].device_count; i++) {
				spi_tx_rx(array_queue[cur_array].calib_result[i]);
			}
			break;
		case SPI_CALIB_SEE_THROUGH:
		case SPI_CALIB_SEE_DARK:
		case SPI_CALIB_OFFSET:
		case SPI_CALIB_CROSS_TALK:
			calib_samples = spi_tx_rx(44); // Get the number of samples to use
			calib_target = spi_tx_rx(45); // Get the distance to the target
			calib_flag = 1;

			break;
		}
		
		EIFR |= (1<<INTF0); // Clear any pending spi interrupts, important because Weiss hand seems to pull ssel low whenever byte is exchanged
	} else {
		spi_tx_rx(0); // Send back 0 if not ready to do SPI transfer (i.e. busy)
	}
	wdt_reset();
}
