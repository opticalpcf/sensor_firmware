#include <avr/io.h>
#include "vl6180x.h"
#include "i2c.h"

#ifndef DEF_SLAVE_ADDR
#define DEF_SLAVE_ADDR 0x52
#endif 

#ifndef TWI_READ
#define TWI_READ 0x1
#endif

#ifndef TWI_WRITE
#define TWI_WRITE 0x0
#endif

#define RANGE_ONLY

/* 
	Initialize a VL6180X sensor
	vl6Ptr: The device to be initialized
	address: The I2C slave address to assign to the device
*/
uint8_t vl6180x_init(vl6180x * vl6Ptr, uint8_t address) {
	char reset;
	
	change_address(vl6Ptr, address); // Changed the device address
	
	vl6Ptr->transmit[0] = vl6Ptr->addr | TWI_WRITE;
	vl6Ptr->transmit[1] = 0x00;
	vl6Ptr->transmit[2] = 0x16;
	vl6Ptr->receive[0] = vl6Ptr->addr | TWI_READ;	
	read_bytes(vl6Ptr,1);
	
	reset = vl6Ptr->receive[1];
	if (1==1){ // Check to see if sensor has already been initialized
		
		// Mandatory : private registers
		write_byte(vl6Ptr,0x0207, 0x01);
		write_byte(vl6Ptr,0x0208, 0x01);
		write_byte(vl6Ptr,0x0096, 0x00);
		write_byte(vl6Ptr,0x0097, 0xfd);
		write_byte(vl6Ptr,0x00e3, 0x00);
		write_byte(vl6Ptr,0x00e4, 0x04);
		write_byte(vl6Ptr,0x00e5, 0x02);
		write_byte(vl6Ptr,0x00e6, 0x01);
		write_byte(vl6Ptr,0x00e7, 0x03);
		write_byte(vl6Ptr,0x00f5, 0x02);
		write_byte(vl6Ptr,0x00d9, 0x05);
		write_byte(vl6Ptr,0x00db, 0xce);
		write_byte(vl6Ptr,0x00dc, 0x03);
		write_byte(vl6Ptr,0x00dd, 0xf8);
		write_byte(vl6Ptr,0x009f, 0x00);
		write_byte(vl6Ptr,0x00a3, 0x3c);
		write_byte(vl6Ptr,0x00b7, 0x00);
		write_byte(vl6Ptr,0x00bb, 0x3c);
		write_byte(vl6Ptr,0x00b2, 0x09);
		write_byte(vl6Ptr,0x00ca, 0x09);
		write_byte(vl6Ptr,0x0198, 0x01);
		write_byte(vl6Ptr,0x01b0, 0x17);
		write_byte(vl6Ptr,0x01ad, 0x00);
		write_byte(vl6Ptr,0x00ff, 0x05);
		write_byte(vl6Ptr,0x0100, 0x05);
		write_byte(vl6Ptr,0x0199, 0x05);
		write_byte(vl6Ptr,0x01a6, 0x1b);
		write_byte(vl6Ptr,0x01ac, 0x3e);
		write_byte(vl6Ptr,0x01a7, 0x1f);
		write_byte(vl6Ptr,0x0030, 0x00);
		
		//Recommended : Public registers - See data sheet for more detail
		write_byte(vl6Ptr,0x0011, 0x10); // Enables polling for �New Sample ready� when measurement completes
		write_byte(vl6Ptr,0x010a, 0x30); // Set the averaging sample period (compromise between lower noise and increased execution time)
		write_byte(vl6Ptr,0x003f, 0x46); // Sets the light and dark gain (upper nibble). Dark gain should not be changed.
		write_byte(vl6Ptr,0x0031, 0xFF); // sets the # of range measurements after which auto calibration of system is performed
//		write_byte(vl6Ptr,0x0040, 0x63); // Set ALS integration time to 100msDocID026571 Rev 1 25/27
		write_byte(vl6Ptr,0x0040, 0x32); // Set ALS integration time to 50ms
		write_byte(vl6Ptr,0x002e, 0x01); // perform a single temperature calibration of the ranging sensor
		
//		write_byte(vl6Ptr,0x003e, 0x31); // Set default ALS inter-measurement period to 500ms
		write_byte(vl6Ptr,0x003e, 0x09); // Set default ALS inter-measurement period to 100ms
		write_byte(vl6Ptr,0x0014, 0x24); // Configures interrupt on �New Sample Ready threshold event�
		#ifdef RANGE_ONLY
			//write_byte(vl6Ptr,0x001b, 0x05); // Set default ranging inter-measurement period to 60ms, might help with crosstalk if separate sensors toggled on carefully?
			write_byte(vl6Ptr,0x001b, 0x02); // Set default ranging inter-measurement period to 30ms
			write_byte(vl6Ptr,0x001c, 0x14); // Set Max Range convergence time to 20ms
		#else
			write_byte(vl6Ptr,0x001b, 0x09); // Set default ranging inter-measurement period to 100ms
			write_byte(vl6Ptr,0x001c, 0x1e); // Set Max Range convergence time to 30ms
			write_byte(vl6Ptr,0x0120, 0x0F); // Scale als result by 15
			write_byte(vl6Ptr,0x02A3, 0x01); // Enable interleaved mode
		#endif

		write_byte(vl6Ptr,0x003f, 0x47); // Set the ALS analog gain to 40
	//	write_byte(vl6Ptr,0x003f, 0x06);
	//	write_byte(vl6Ptr,0x0021,0x00);  // Set valid height measurement to 0?
	//	write_byte(vl6Ptr,0x0025,0x00);

		write_byte(vl6Ptr,0x002d, 0x13); // Enable range ignore
		write_byte(vl6Ptr,0x0025, 0xFF); // Set range ignore height to 255
	}
	return vl6Ptr->addr;
}

/*
	Change the I2C slave address of the device
	vlPtr: The device whose address should be changed
	new_addr: The desired I2C address
*/
uint8_t change_address(vl6180x * vl6Ptr, uint8_t new_addr)
{
	// Try to read from new address
	vl6Ptr->transmit[0] = new_addr | TWI_WRITE;
	vl6Ptr->transmit[1] = 0x02;
	vl6Ptr->transmit[2] = 0x12;
	vl6Ptr->receive[0] = new_addr | TWI_READ;	
	read_bytes(vl6Ptr,1);
	
	vl6Ptr->addr = (vl6Ptr->receive[1]) << 1;
	// If it worked, then address has already been changed
	if(vl6Ptr->addr == new_addr)
	{
		return vl6Ptr->addr;
	}
	// Try to write new address using default slave address
	vl6Ptr->addr = DEF_SLAVE_ADDR |TWI_WRITE;
	write_byte(vl6Ptr, 0x0212, new_addr >> 1);
	
	// Read the address back
	vl6Ptr->transmit[0] = new_addr | TWI_WRITE;
	vl6Ptr->transmit[1] = 0x02;
	vl6Ptr->transmit[2] = 0x12;
	vl6Ptr->receive[0] = new_addr | TWI_READ;
	read_bytes(vl6Ptr,1);
	
	// Set the received address and return it
	vl6Ptr->addr = (vl6Ptr->receive[1]) << 1;
	return vl6Ptr->addr;
	
}

/*
	Toggle measurement mode on and off
	vl6Ptr: The device whose measurement should be toggled
*/
void toggle_measurement(vl6180x * vl6Ptr) {

	#ifdef RANGE_ONLY
		vl6Ptr->transmit[0] = vl6Ptr->addr | TWI_WRITE;
		// Indicate write to SYSRANGE_START (0x18)
		vl6Ptr->transmit[1] = 0x00;
		vl6Ptr->transmit[2] = 0x18;
		// Write 0x03 to SYSRANGE_START for continuous measurements
		vl6Ptr->transmit[3] = 0x03;
		write_bytes(vl6Ptr,4);
	#else
		vl6Ptr->transmit[0] = vl6Ptr->addr | TWI_WRITE;
		// Indicate write to SYSALS_START (0x38)
		vl6Ptr->transmit[1] = 0x00;
		vl6Ptr->transmit[2] = 0x38;
		// Write 0x03 to SYSALS_START for continuous measurements
		vl6Ptr->transmit[3] = 0x03;
		write_bytes(vl6Ptr,4);
	#endif
}

/*
// Initialize interleave mode
void start_interleave(vl6180x * vl6Ptr) 
{
	
	vl6Ptr->transmit[0] = vl6Ptr->addr | TWI_WRITE;
	// Indicate write to INTER_LEAVED_MODE_ENABLE (0x02A3)
	vl6Ptr->transmit[1] = 0x02;
	vl6Ptr->transmit[2] = 0xA3;
	// Activate Interleaved mode
	vl6Ptr->transmit[3] = 0x01;
	write_bytes(vl6Ptr,4);
	
	vl6Ptr->transmit[0] = vl6Ptr->addr | TWI_WRITE;
	// Indicate write to SYSALS_START (0x18)
	vl6Ptr->transmit[1] = 0x00;
	vl6Ptr->transmit[2] = 0x38;
	// Write 0x03 to SYSRANGE_START for continuous measurements
	vl6Ptr->transmit[3] = 0x03;
	write_bytes(vl6Ptr,4);
	
}
*/

/*
// Get data from vl6Ptr. Set range_meas to 1 to get a range measurement, 0 for ALS measurement
// Returns 0 if the new and previous measurement were equal
uint8_t get_data(vl6180x * vl6Ptr, uint8_t range_meas)
{
	vl6Ptr->transmit[0] = vl6Ptr->addr | TWI_WRITE;
	uint16_t old_meas = 0;		
	if(range_meas)
	{
		// Indicate read from RESULT_RANGE_VAL (0x62)
		vl6Ptr->transmit[1] = 0x00;
		vl6Ptr->transmit[2] = 0x62;
		vl6Ptr->receive[0] = vl6Ptr->addr | TWI_READ;			
		read_bytes(vl6Ptr,1);
		
		old_meas = vl6Ptr->distance;
		vl6Ptr->distance = vl6Ptr->receive[1];
		return vl6Ptr->distance != old_meas;
		
	}
	else
	{
		// Indicate read from RESULT_ALS_VAL (0x50)
		vl6Ptr->transmit[1] = 0x00;
		vl6Ptr->transmit[2] = 0x50;
					
	
		vl6Ptr->receive[0] = vl6Ptr->addr | TWI_READ;
					
		read_bytes(vl6Ptr,2);
		old_meas = vl6Ptr->als;
		vl6Ptr->als = (vl6Ptr->receive[1] << 8) | vl6Ptr->receive[2];
		return vl6Ptr->als != old_meas;
	}
				
}
*/
/*

void set_register(vl6180x* vl6Ptr, uint8_t reg_addr, uint8_t* reg_data, uint8_t data_length) {
	vl6Ptr->transmit[0] = vl6Ptr->addr | TWI_WRITE;
	
	vl6Ptr->transmit[1] = 0x00;
	vl6Ptr->transmit[2] = reg_addr;
	
	for(int i = 0; i < data_length; i++) {
		vl6Ptr->transmit[i+3] = reg_data[i];
	}

	write_bytes(vl6Ptr, 3+data_length);
}

void read_register(vl6180x* vl6Ptr, uint8_t reg_addr, uint8_t* reg_data, uint8_t data_length) {

	vl6Ptr->transmit[0] = vl6Ptr->addr | TWI_WRITE;
	vl6Ptr->transmit[1] = 0x00;
	vl6Ptr->transmit[2] = reg_addr;

	vl6Ptr->receive[0] = vl6Ptr->addr | TWI_READ;
	read_bytes(vl6Ptr, data_length);

	for(int i = 0; i < data_length; i++) {
		reg_data[i] = vl6Ptr->receive[i+1];
	}

}
*/
/*
void enable_sensitivity(vl6180x* vl6Ptr) {
	vl6Ptr->transmit[0] = vl6Ptr->addr | TWI_WRITE;
	// Indicate write to SYSRANGE__RANGE_CHECK_ENABLES (0x2D)
	vl6Ptr->transmit[1] = 0x00;
	vl6Ptr->transmit[2] = 0x2D;
	// Write value
	vl6Ptr->transmit[3] = 0x13;
	write_bytes(vl6Ptr, 4);

	
	//vl6Ptr->transmit[0] = vl6Ptr->addr | TWI_WRITE;
	// Indicate write to SYSRANGE__RANGE_IGNORE_VALID_HEIGHT (0x25)
	//vl6Ptr->transmit[1] = 0x00;
	//vl6Ptr->transmit[2] = 0x25;
	// Write value
	//vl6Ptr->transmit[3] = 0xFF;
	//write_bytes(vl6Ptr, 4);
	
}

void set_sensitivity(vl6180x* vl6Ptr, uint8_t sensitivity_msb, uint8_t sensitivty_lsb) {

	vl6Ptr->transmit[0] = vl6Ptr->addr | TWI_WRITE;
	// Indicate write to SYSRANGE__RANGE_IGNORE_THRESHOLD (0x26)
	vl6Ptr->transmit[1] = 0x00;
	vl6Ptr->transmit[2] = 0x26;
	// Write value
	vl6Ptr->transmit[3] = sensitivity_msb;
	vl6Ptr->transmit[4] = sensitivty_lsb;
	write_bytes(vl6Ptr,5);

}
*/